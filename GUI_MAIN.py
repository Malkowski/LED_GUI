import sys
from PyQt4 import QtCore, QtGui

import LED_GUI_PY
import serial


class Uart():
    def __init__(self):
        self.ser = serial.Serial()
        self.ser.baudrate = 115200
        self.ser.port = "/dev/ttyUSB0"
        self.ser.open()

        print(self.ser.portstr)

    def __exit__(self):
        self.ser.close()

    def open_port(self):
        pass

    def close_port(self):
        pass

    def send_data(self, data):
        self.ser.write(str(data) + '\n\r')
        #self.ser.write(str(data))

class GuiMain(QtGui.QWidget, LED_GUI_PY.Ui_Form):
    def __init__(self, parent=None):
        super(GuiMain, self).__init__(parent)
        self.setupUi(self)
        self.uart = Uart()

        # Setup Red Slider
        self.slider_red.setMinimum(0)
        self.slider_red.setMaximum(100)
        self.slider_red.setValue(20)
        self.slider_red.setTickPosition(QtGui.QSlider.TicksBelow)
        self.slider_red.setTickInterval(1)
        self.slider_red.valueChanged.connect(self.v_change)
        # Setup Blue Slider
        self.slider_blue.setMinimum(0)
        self.slider_blue.setMaximum(100)
        self.slider_blue.setValue(20)
        self.slider_blue.setTickPosition(QtGui.QSlider.TicksBelow)
        self.slider_blue.setTickInterval(1)
        self.slider_blue.valueChanged.connect(self.v_change)
        # Setup Green Slider
        self.slider_green.setMinimum(0)
        self.slider_green.setMaximum(100)
        self.slider_green.setValue(20)
        self.slider_green.setTickPosition(QtGui.QSlider.TicksBelow)
        self.slider_green.setTickInterval(1)
        self.slider_green.valueChanged.connect(self.v_change)
        # Setup Slider Sinus
        self.slider_sinus.setMinimum(0)
        self.slider_sinus.setMaximum(100)
        self.slider_sinus.setValue(0)
        self.slider_sinus.setTickPosition(QtGui.QSlider.TicksBelow)
        self.slider_sinus.setTickInterval(1)
        self.slider_sinus.valueChanged.connect(self.v_change)
        self.slider_sinus.valueChanged.connect(self.lcdNumber.display)

        # VARIABLES
        self.sin_first_on = 0
        self.RED_VALUE = 20
        self.GREEN_VALUE = 20
        self.BLUE_VALUE = 20
        self.SIN_VALUE = 0

        QtCore.QObject.connect(self.pushButton_red, QtCore.SIGNAL('clicked()'), self.reset_slider_value)
        QtCore.QObject.connect(self.pushButton_green, QtCore.SIGNAL('clicked()'), self.reset_slider_value)
        QtCore.QObject.connect(self.pushButton_blue, QtCore.SIGNAL('clicked()'), self.reset_slider_value)
        QtCore.QObject.connect(self.pushButton_sinus, QtCore.SIGNAL('clicked()'), self.reset_slider_value)
        # self.pushButton_red.clicked.connect(self.reset_slider_red_value)

    def keyPressEvent(self, e):
        if e.key() == QtCore.Qt.Key_Escape:
            self.close()

    def send_value(self, slider_type, **options):

        if options.get("Attribute") == "red":
            slider_attribute = " RED"
        elif options.get("Attribute") == "green":
            slider_attribute = " GRN"
        elif options.get("Attribute") == "blue":
            slider_attribute = " BLU"
        elif options.get("Attribute") == "sinus":
            slider_attribute = " SIN"
        else:
            slider_attribute = " ---"

        if slider_type <= 9:
            self.uart.send_data("0" + str(slider_type) + slider_attribute)
        else:
            self.uart.send_data(str(slider_type) + slider_attribute)

    def v_change(self):
        if self.sender() == self.slider_red:
            self.RED_VALUE = self.sender().value()
            self.send_value(self.RED_VALUE, Attribute="red")

        elif self.sender() == self.slider_green:
            self.GREEN_VALUE = self.sender().value()
            self.send_value(self.GREEN_VALUE, Attribute="green")

        elif self.sender() == self.slider_blue:
            self.BLUE_VALUE = self.sender().value()
            self.send_value(self.BLUE_VALUE, Attribute="blue")

        elif self.sender() == self.slider_sinus:
            self.SIN_VALUE = self.sender().value()
            self.send_value(self.SIN_VALUE, Attribute="sinus")

        else:
            print("Wrong input")

    def reset_slider_value(self):
        # RED RESET
        if self.sender() == self.pushButton_red:
            if self.slider_red.value() == 0:
                self.slider_red.setValue(self.saved_value_red)

            else:
                self.saved_value_red = self.slider_red.value()
                self.slider_red.setValue(0)

        # GREEN RESET
        if self.sender() == self.pushButton_green:
            if self.slider_green.value() == 0:
                self.slider_green.setValue(self.saved_value_green)
            else:
                self.saved_value_green = self.slider_green.value()
                self.slider_green.setValue(0)

        # BLUE RESET
        if self.sender() == self.pushButton_blue:
            if self.slider_blue.value() == 0:
                self.slider_blue.setValue(self.saved_value_blue)
            else:
                self.saved_value_blue = self.slider_blue.value()
                self.slider_blue.setValue(0)

        # SINUS RESET
        if self.sender() == self.pushButton_sinus:

            if (self.slider_sinus.value() == 0) & (self.sin_first_on == 0):
                self.sin_first_on = 1
                self.slider_sinus.setValue(20)

            if (self.slider_sinus.value() == 0) & (self.sin_first_on == 1):
                self.slider_sinus.setValue(self.saved_value_sinus)
            else:
                self.saved_value_sinus = self.slider_sinus.value()
                self.slider_sinus.setValue(0)


def main():
    app = QtGui.QApplication(sys.argv)
    form = GuiMain()
    form.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
