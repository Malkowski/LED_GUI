# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'LED_GUI_PROJEKT.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)


class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName(_fromUtf8("Form"))
        Form.resize(496, 403)
        self.horizontalLayout_2 = QtGui.QHBoxLayout(Form)
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.gridLayout = QtGui.QGridLayout()
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.pushButton_red = QtGui.QPushButton(Form)
        self.pushButton_red.setObjectName(_fromUtf8("pushButton_red"))
        self.gridLayout.addWidget(self.pushButton_red, 5, 1, 1, 1)
        self.pushButton_green = QtGui.QPushButton(Form)
        self.pushButton_green.setObjectName(_fromUtf8("pushButton_green"))
        self.gridLayout.addWidget(self.pushButton_green, 5, 3, 1, 1)
        self.label_red = QtGui.QLabel(Form)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Meera"))
        font.setPointSize(20)
        self.label_red.setFont(font)
        self.label_red.setObjectName(_fromUtf8("label_red"))
        self.gridLayout.addWidget(self.label_red, 1, 1, 1, 1)
        self.slider_blue = QtGui.QSlider(Form)
        self.slider_blue.setOrientation(QtCore.Qt.Vertical)
        self.slider_blue.setObjectName(_fromUtf8("slider_blue"))
        self.gridLayout.addWidget(self.slider_blue, 2, 5, 1, 1)
        self.slider_red = QtGui.QSlider(Form)
        self.slider_red.setOrientation(QtCore.Qt.Vertical)
        self.slider_red.setObjectName(_fromUtf8("slider_red"))
        self.gridLayout.addWidget(self.slider_red, 2, 1, 1, 1)
        self.slider_sinus = QtGui.QSlider(Form)
        self.slider_sinus.setOrientation(QtCore.Qt.Vertical)
        self.slider_sinus.setObjectName(_fromUtf8("slider_sinus"))
        self.gridLayout.addWidget(self.slider_sinus, 2, 7, 1, 1)
        self.pushButton_sinus = QtGui.QPushButton(Form)
        self.pushButton_sinus.setObjectName(_fromUtf8("pushButton_sinus"))
        self.gridLayout.addWidget(self.pushButton_sinus, 5, 7, 1, 1)
        self.slider_green = QtGui.QSlider(Form)
        self.slider_green.setOrientation(QtCore.Qt.Vertical)
        self.slider_green.setObjectName(_fromUtf8("slider_green"))
        self.gridLayout.addWidget(self.slider_green, 2, 3, 1, 1)
        self.pushButton_blue = QtGui.QPushButton(Form)
        self.pushButton_blue.setObjectName(_fromUtf8("pushButton_blue"))
        self.gridLayout.addWidget(self.pushButton_blue, 5, 5, 1, 1)
        self.label_sinus = QtGui.QLabel(Form)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Meera"))
        font.setPointSize(20)
        self.label_sinus.setFont(font)
        self.label_sinus.setObjectName(_fromUtf8("label_sinus"))
        self.gridLayout.addWidget(self.label_sinus, 1, 7, 1, 1)
        self.label_green = QtGui.QLabel(Form)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Meera"))
        font.setPointSize(20)
        self.label_green.setFont(font)
        self.label_green.setObjectName(_fromUtf8("label_green"))
        self.gridLayout.addWidget(self.label_green, 1, 3, 1, 1)
        self.lcdNumber = QtGui.QLCDNumber(Form)
        self.lcdNumber.setObjectName(_fromUtf8("lcdNumber"))
        self.gridLayout.addWidget(self.lcdNumber, 4, 7, 1, 1)
        self.label_blue = QtGui.QLabel(Form)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Meera"))
        font.setPointSize(20)
        self.label_blue.setFont(font)
        self.label_blue.setObjectName(_fromUtf8("label_blue"))
        self.gridLayout.addWidget(self.label_blue, 1, 5, 1, 1)
        self.line = QtGui.QFrame(Form)
        self.line.setFrameShape(QtGui.QFrame.VLine)
        self.line.setFrameShadow(QtGui.QFrame.Sunken)
        self.line.setObjectName(_fromUtf8("line"))
        self.gridLayout.addWidget(self.line, 2, 2, 1, 1)
        self.line_3 = QtGui.QFrame(Form)
        self.line_3.setFrameShape(QtGui.QFrame.VLine)
        self.line_3.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_3.setObjectName(_fromUtf8("line_3"))
        self.gridLayout.addWidget(self.line_3, 2, 6, 1, 1)
        self.line_2 = QtGui.QFrame(Form)
        self.line_2.setFrameShape(QtGui.QFrame.VLine)
        self.line_2.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_2.setObjectName(_fromUtf8("line_2"))
        self.gridLayout.addWidget(self.line_2, 2, 4, 1, 1)
        spacerItem = QtGui.QSpacerItem(5, 10, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Fixed)
        self.gridLayout.addItem(spacerItem, 0, 1, 1, 1)
        spacerItem1 = QtGui.QSpacerItem(20, 10, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Fixed)
        self.gridLayout.addItem(spacerItem1, 6, 3, 1, 1)
        self.line_4 = QtGui.QFrame(Form)
        self.line_4.setFrameShape(QtGui.QFrame.VLine)
        self.line_4.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_4.setObjectName(_fromUtf8("line_4"))
        self.gridLayout.addWidget(self.line_4, 2, 0, 1, 1)
        self.checkBox_red = QtGui.QCheckBox(Form)
        self.checkBox_red.setObjectName(_fromUtf8("checkBox_red"))
        self.gridLayout.addWidget(self.checkBox_red, 4, 1, 1, 1)
        self.checkBox_green = QtGui.QCheckBox(Form)
        self.checkBox_green.setObjectName(_fromUtf8("checkBox_green"))
        self.gridLayout.addWidget(self.checkBox_green, 4, 3, 1, 1)
        self.checkBox_blue = QtGui.QCheckBox(Form)
        self.checkBox_blue.setObjectName(_fromUtf8("checkBox_blue"))
        self.gridLayout.addWidget(self.checkBox_blue, 4, 5, 1, 1)
        self.horizontalLayout_2.addLayout(self.gridLayout)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        Form.setWindowTitle(_translate("Form", "LED BORDELLO CREATORE", None))
        self.pushButton_red.setText(_translate("Form", "On/Off", None))
        self.pushButton_green.setText(_translate("Form", "On/Off", None))
        self.label_red.setText(_translate("Form", "RED", None))
        self.pushButton_sinus.setText(_translate("Form", "On/Off", None))
        self.pushButton_blue.setText(_translate("Form", "On/Off", None))
        self.label_sinus.setText(_translate("Form", "SINUS", None))
        self.label_green.setText(_translate("Form", "GREEN", None))
        self.label_blue.setText(_translate("Form", "BLUE", None))
        self.checkBox_red.setText(_translate("Form", "Sinus Red", None))
        self.checkBox_green.setText(_translate("Form", "Sinus Green", None))
        self.checkBox_blue.setText(_translate("Form", "Sinus Blue", None))

